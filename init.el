;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Taylor's Emacs config
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; general configuration
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; no backup files
;; refresh files that have changed outside emacs
;; use the newest elisp bytecode
(setq backup-inhibited t)
(setq auto-save-default nil)
(global-auto-revert-mode t)
(setq load-prefer-newer t)

;; smooth scrolling
(setq scroll-conservatively 10000)
(setq scroll-preserve-screen-position t)

;; trim whitespace before saving
;; also require a newline at the end of files
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(setq require-final-newline t)

;; show parens
(show-paren-mode 1)

;; unique buffer names
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; ediff in same window
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; put all of those dumb custom variables in another file
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; melpa
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; setup melpa
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-verbose t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package company
  :init
  (add-hook 'after-init-hook 'global-company-mode))

(use-package magit
  :ensure t
  :defer 0.5
  :config
  (global-set-key (kbd "C-x g") 'magit-status))

(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-copy-env "PATH"))

(use-package json-mode
  :ensure t)

(use-package multi-term
  :ensure t
  :config
  (setq multi-term-program "/bin/bash")
  :bind
  (("M-s M-s" . multi-term)
   ("M-s M-f" . multi-term-next)
   ("M-s M-b" . multi-term-prev)
   ("M-s M-t" . multi-term-dedicated-toggle)))

(use-package restclient
  :ensure t
  :config
  (require 'restclient))

(use-package kotlin-mode
  :ensure t)

(use-package htmlize
  :ensure t)

(use-package ztree
  :ensure t)

(use-package visual-regexp
  :ensure t
  :bind
  (("C-c r" . vr/replace)
   ("C-c q" . vr/query-replace)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ivy/counsel/swiper
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package ivy
  :ensure t
  :defer 0.1
  :diminish
  :config
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-display-style 'fancy)
  (ivy-mode 1))

(use-package counsel
  :ensure t
  :after ivy
  :config
  (counsel-mode)
  :bind
  (("C-x j" . counsel-git-grep)))

(use-package swiper
  :ensure t
  :config
  (setq swiper-goto-start-of-match t)
  :bind
  (("C-s" . swiper)
   ("C-r" . swiper-backward)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dired
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; recursively copy/delete folders
;; remove all of the details from the file listings
(use-package dired
  :config
  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always))

;; use the subtree package for better visual nav
(use-package dired-subtree
  :ensure t
  :bind (:map dired-mode-map
	      ("i" . dired-subtree-insert)
	      (";" . dired-subtree-remove)
	      ("<tab>" . dired-subtree-toggle)
	      ("<backtab>" . dired-subtree-cycle)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; org/agenda dir
(setq org-agenda-files '("~/orgs/"))

;; custom agenda view
(setq org-agenda-custom-commands
      '(("c" "Simple agenda view"
	 ((agenda "")
	  (alltodo "")))))

;; enable markdown exporting
(eval-after-load "org"
  '(require 'ox-md nil t))

;; disable toc and section numbering and validate link
(setq org-export-with-section-numbers nil)
(setq org-export-with-toc nil)
(setq org-html-validation-link nil)

;; org todo states
(setq org-todo-keywords
      '((sequence "TODO" "IN-PROGRESS" "|" "DONE" "ON-HOLD")))

;; colors for todo states
(setq org-todo-keyword-faces
      '(("TODO" . modus-theme-subtle-red)
	("IN-PROGRESS" . modus-theme-refine-yellow)
	("ON-HOLD" . modus-theme-special-cold)))

;; make child todos/checkboxes dependent
(setq org-enforce-todo-dependencies t)

;; slide presentations for org mode
(use-package org-present
  :ensure t
  :init
  (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-display-inline-images)
                 (org-present-hide-cursor)
                 (org-present-read-only)))
  (add-hook 'org-present-mode-quit-hook
	    (lambda ()
	      (org-present-small)
	      (org-remove-inline-images)
	      (org-present-show-cursor)
	      (org-present-read-write)))
  :config
  (setq org-present-text-scale 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; python
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package elpy
  :ensure t
  :defer 0.7
  :config
  (setq python-shell-interpreter "/Library/Frameworks/Python.framework/Versions/3.7/bin/python3")
  (setq elpy-rpc-python-command "/Library/Frameworks/Python.framework/Versions/3.7/bin/python3")
  (elpy-enable))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; c programming
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; indent 4 spaces in normal people style c
(setq c-default-style "linux"
      c-basic-offset 4)

;; make sure arduino files get highlighted as cpp
(add-to-list 'auto-mode-alist '("\\.ino\\'" . c-mode))

;; c irony
(use-package irony
  :ensure t
  :config
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

;; load the company irony backend after company mode loads up
(use-package company-irony
  :ensure t
  :config
  (add-hook 'irony-mode-hook 'company-irony-setup-begin-commands))

(setq company-backends (delete 'company-semantic company-backends))
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony))

;; load up flycheck for use with irony
(use-package flycheck
  :ensure t
  :config
  (add-hook 'c-mode-hook 'flycheck-mode))

(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; visuals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; no menus or toolbars
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq ns-use-proxy-icon nil)
(setq frame-title-format nil)
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . light))

;; startup
(setq inhibit-splash-screen t)
(setq initial-scratch-message "")

;; no bells
(setq ring-bell-function 'ignore)

;; highlight the currently selected line
(global-hl-line-mode 1)

;; display column and time in mode line
(display-time-mode 1)
(column-number-mode 1)

;; theme
(use-package modus-operandi-theme
  :ensure t
  :init
  (setq modus-operandi-theme-slanted-constructs t)
  (setq modus-operandi-theme-bold-constructs t)
  (setq modus-operandi-theme-proportional-fonts t)
  (setq modus-operandi-theme-scale-headings t)
  (setq modus-operandi-theme-scale-1 1.05)
  (setq modus-operandi-theme-scale-2 1.1)
  (setq modus-operandi-theme-scale-3 1.25)
  (setq modus-operandi-theme-scale-4 1.5)
  :config
  (load-theme 'modus-operandi t))

;; modeline
(use-package powerline
  :ensure t
  :config
  (setq powerline-display-buffer-size nil)
  (setq powerline-display-mule-info nil)
  (setq powerline-display-hud nil)
  (custom-set-faces
   '(powerline-active1 ((t (:background "#c4ede0" :foreground "#191919")))))
  (powerline-default-theme))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; mu4e smtpmail
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; this mu4e setup requires offlineimap + mu + gnutls are installed
;;
;; for macos:
;;   - brew install offlineimap
;;   - brew install mu
;;   - bres install gnutls
;;
;; 1. create an app password in gmail
;; 2. setup .offlineimaprc
;; 3. run `offlineimap -o` and wait
;; 4. create an .authinfo file for smtpmail

;; add mu4e to the load path since it was installed with mu
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu/mu4e/")

(setq taylor/email-signature "-- Taylor")

(use-package mu4e
  :ensure nil
  :defer 1.0
  :config
  (add-hook 'mu4e-compose-mode-hook 'turn-off-auto-fill)
  :custom
  (message-cite-function  'message-cite-original)
  (message-citation-line-function  'message-insert-formatted-citation-line)
  (message-cite-reply-position 'above)
  (message-yank-prefix  "    ")
  (message-yank-cited-prefix  "    ")
  (message-yank-empty-prefix  "    ")
  (message-citation-line-format "On %e %B %Y %R, %f wrote:\n")
  (mu4e-sent-messages-behavior 'delete)
  (mu4e-maildir "/Users/taylor/mail/gmail")
  (mu4e-attachment-dir "/Users/taylor/Downloads")
  (mu4e-drafts-folder "/[Gmail].Drafts")
  (mu4e-sent-folder "/[Gmail].Sent Mail")
  (mu4e-refile-folder "/[Gmail].All Mail")
  (mu4e-trash-folder "/[Gmail].Trash")
  (mu4e-get-mail-command "offlineimap -o")
  (mu4e-update-interval nil)
  (mu4e-use-fancy-chars t)
  (mu4e-show-addresses t)
  (mu4e-show-images t)
  (mu4e-headers-include-related nil)
  (mu4e-compose-signature taylor/email-signature)
  (mu4e-maildir-shortcuts
   '(("/INBOX" . ?i)))
  (user-mail-agent 'mu4e-user-agent)
  (mu4e-view-prefer-html t)
  :bind
  (("C-x m" . mu4e)))

;; setup user info
(setq user-mail-address "{EMAIL ADDRESS}")
(setq user-full-name "Taylor")

;; use smtpmail for sending
;; dont forget to create an .authinfo file
(use-package smtpmail
  :ensure nil
  :custom
  (message-send-mail-function 'smtpmail-send-it)
  (smtpmail-stream-type 'starttls)
  (smtpmail-default-smtp-server "smtp.gmail.com")
  (smtpmail-smtp-server "smtp.gmail.com")
  (smtpmail-smtp-service 587))
